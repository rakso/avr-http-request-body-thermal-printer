#ifndef HTTPBODYPARSER_H_
#define HTTPBODYPARSER_H_

#include "Ethernet/src/Ethernet.h"

typedef void (*vFunctionCall)(EthernetClient& tcpClient);

class HttpBodyReader {
 private:
  EthernetClient& tcpClient;
  uint8_t* ethRequestBufferPointer;

  boolean requestHasBody      = false;
  boolean isNewRequest        = false;
  uint16_t bufferLength       = 0;
  uint16_t headIndexForBuffer = 0;

  // bool appendHeaderToBody = true;
  bool appendHeaderToBody = false;

  bool readyToReply     = false;
  bool waitingPostReply = false;

  vFunctionCall otherFunc;

  void sendResponse();
  void closeTcpConnnection();

 public:
  HttpBodyReader(EthernetClient& tcpClient, uint8_t* ethRequestBufferPointer);
  ~HttpBodyReader();

  void setFunc(vFunctionCall funcToSet);

  void update();
  bool hasNewRequestBody();
  bool hasNewRequest();
  uint8_t* getTheBuffer();
  uint16_t getTheBufferLength();

  bool isReadyToReply();
  void doReply(uint16_t dataLength);
  void doRequest(uint16_t dataLength, IPAddress hostAddress, uint16_t hostPort);
};

void HttpBodyReader::setFunc(vFunctionCall funcToSet) {
  this->otherFunc = funcToSet;
}

void HttpBodyReader::sendResponse() {
  this->otherFunc(tcpClient);
}

HttpBodyReader::HttpBodyReader(EthernetClient& tcpClient,
                               uint8_t* ethRequestBufferPointer)
    : tcpClient(tcpClient), ethRequestBufferPointer(ethRequestBufferPointer) {}

HttpBodyReader::~HttpBodyReader() {}

void HttpBodyReader::update() {
  if (this->waitingPostReply || tcpClient) {
    Serial.println(F("new tcpClient or waiting post reply"));
    Serial.println(F("HTTP HEADER: "));

    uint16_t requestContentLength = 0;
    char previousChar;
    boolean eolReached       = false;
    boolean headerDataEnded  = false;
    uint16_t lengthOfTheLine = 0;
    uint16_t currentIndex    = 0;
    this->headIndexForBuffer = 0;
    char ethRequestLineBuffer[100];

    while (tcpClient.connected()) {
      if (tcpClient.available()) {
        char c = tcpClient.read();

        if (appendHeaderToBody)
          ethRequestBufferPointer[headIndexForBuffer++] = c;

        // Serial.write(c);

        // We've got whole line of data
        if (c == '\n' && previousChar == '\r') {
          // Because the last char is '\r' - we don't require it
          currentIndex -= 1;
          lengthOfTheLine = currentIndex;
          currentIndex    = 0;  // Let's reset it
          // Indicate that this is the end of the string
          // ethRequestLineBuffer[lengthOfTheLine] = '\0';
          eolReached = true;
        } else {
          previousChar                         = c;
          ethRequestLineBuffer[currentIndex++] = c;
        }

        // Do action with fetched line data
        if (eolReached) {
          eolReached = false;

          if (lengthOfTheLine == 0) {
            // If last "data" in the line was empty then we know that this was
            // end of the header
            headerDataEnded = true;
            readyToReply    = true;
            Serial.println("ready to reply");
            break;
          }
          
          // 15 is the length of "content-length:" words
          else if (strncmp(ethRequestLineBuffer, "Content-Length:", 15) == 0 ||
                   strncmp(ethRequestLineBuffer, "content-length:", 15) == 0) {
            // 16 is the index of the content length value beggining
            requestContentLength = atoi(ethRequestLineBuffer + 16);
          }
        }
      }
    }

    if (headerDataEnded) {
      isNewRequest = true;

      if (requestContentLength > 0) {
        bufferLength = 0;
        for (uint16_t i = 0; i < requestContentLength; i++) {
          bufferLength                          = this->headIndexForBuffer + i;
          uint8_t c                                = tcpClient.read();
          ethRequestBufferPointer[bufferLength] = c;
        }
        ethRequestBufferPointer[++bufferLength] = '\0';
        requestHasBody                          = true;
      }
    }
  }
}

void HttpBodyReader::closeTcpConnnection() {
  // give the web browser time to receive the data
  delay(1);
  // close the connection:
  tcpClient.stop();
  Serial.println("tcpClient disconnected");
}

bool HttpBodyReader::hasNewRequestBody() {
  return this->requestHasBody;
}

uint8_t* HttpBodyReader::getTheBuffer() {
  this->requestHasBody = false;
  return this->ethRequestBufferPointer;
}

bool HttpBodyReader::hasNewRequest() {
  bool returnValue   = this->isNewRequest;
  this->isNewRequest = false;

  return returnValue;
}

uint16_t HttpBodyReader::getTheBufferLength() {
  return this->bufferLength;
}

bool HttpBodyReader::isReadyToReply() {
  return this->readyToReply;
}

void HttpBodyReader::doReply(uint16_t dataLength) {
  // this->sendResponse();
  Serial.print("Response sent: ");
  if (tcpClient.write(ethRequestBufferPointer, dataLength)) {
    this->readyToReply = false;
    this->closeTcpConnnection();
    Serial.println("OK");
  } else {
    Serial.println("NOK");
  }
  return;
}

void HttpBodyReader::doRequest(uint16_t dataLength,
                               IPAddress hostAddress,
                               uint16_t hostPort) {
  Serial.println("connecting to ERCC...");

  if (tcpClient.connect(hostAddress, hostPort)) {
    Serial.println("connected to ERCC");
    // tcpClient.println("GET /search?q=arduino HTTP/1.0");
    // tcpClient.println();
    Serial.print("POST sent: ");
    if (tcpClient.write(ethRequestBufferPointer, dataLength)) {
      Serial.println("OK");
      this->waitingPostReply = true;
    } else {
      Serial.println("NOK");
    }
  } else {
    Serial.println("connection failed");
  }
}

#endif