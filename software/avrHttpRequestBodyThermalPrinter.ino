#define DEVICE_NAME      "AVR HTTP request thermal printer"
#define HARDWARE_VERSION 1
uint16_t softwareVersion[3] = {1, 0, 0};

#include <SPI.h>
#include "src/Ethernet/src/Ethernet.h"
#include "src/httpBodyReader.h"

byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};
IPAddress ip(192, 168, 1, 188);

EthernetServer server(80);
unsigned int udpPort = 9811;  // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  // buffer to hold incoming packet,
char ReplyBuffer[] = "acknowledged";        // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP udp;

EthernetClient tcpClient;
uint8_t ethRequestBufferPointer[1024];
HttpBodyReader httpBody(tcpClient, ethRequestBufferPointer);

void sendHttpResponse(EthernetClient tcpClient) {
  // send a standard http response header
  tcpClient.println("HTTP/1.1 200 OK");
  tcpClient.println("Content-Type: text/html");
  tcpClient.println("Connection: close");  // the connection will be closed
                                           // after completion of the
                                           //  response
  tcpClient.println(
      "Refresh: 5");  // refresh the page automatically every 5 sec
  tcpClient.println();
  tcpClient.println("<!DOCTYPE HTML>");
  tcpClient.println("<html>");
  // output the value of each analog input pin
  for (int analogChannel = 0; analogChannel < 6; analogChannel++) {
    int sensorReading = analogRead(analogChannel);
    tcpClient.print("analog input ");
    tcpClient.print(analogChannel);
    tcpClient.print(" is ");
    tcpClient.print(sensorReading);
    tcpClient.println("<br />");
  }
  tcpClient.println("</html>");
}

void setup() {
  // Serial.begin(115200);
  Serial.begin(9600);

  Serial.print(F("Device: \""));
  Serial.print(F(DEVICE_NAME));
  Serial.print(F("\" SV: "));
  for (uint16_t i = 0; i < 3; i++) {
    Serial.print(softwareVersion[i]);
    if (i < 2) {
      Serial.print(F("."));
    } else {
      Serial.print(F(" Hardware: "));
    }
  }
  Serial.println(HARDWARE_VERSION);

  Ethernet.init(12);  // UniversalPuzzleController

  Serial.println("Trying to get an IP address using DHCP");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");

    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println(
          "Ethernet shield was not found.  Sorry, can't run without hardware. "
          ":(");
      while (true) {
        delay(1);  // do nothing, no point running without Ethernet hardware
      }
    }

    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }

    // initialize the Ethernet device not using DHCP:
    Ethernet.begin(mac, ip);
  }

  // start listening for clients on TCP and UDP
  server.begin();
  udp.begin(udpPort);

  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());

  httpBody.setFunc((vFunctionCall)sendHttpResponse);
}

void loop() {
  uint32_t udpPacketSize = udp.parsePacket();
  tcpClient              = server.available();

  httpBody.update();

  if (httpBody.hasNewRequestBody()) {
    for (uint16_t i = 0; i < httpBody.getTheBufferLength(); i++) {
      Serial.print((char)httpBody.getTheBuffer()[i]);
    }
    Serial.println();
    Serial.println();
    if (httpBody.isReadyToReply())
    {
      ethRequestBufferPointer[0] = 'O';
      ethRequestBufferPointer[1] = 'K';
      ethRequestBufferPointer[2] = '!';
      httpBody.doReply(3);
    }
    
  }

  if (udpPacketSize) {
    Serial.print("Received packet of size ");
    Serial.println(udpPacketSize);
    Serial.print("From ");
    IPAddress remote = udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(udp.remotePort());

    // read the packet into packetBufffer
    udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    // send a reply to the IP address and port that sent us the packet we
    // received
    udp.beginPacket(udp.remoteIP(), udp.remotePort());
    udp.write(ReplyBuffer);
    udp.endPacket();
  }

  switch (Ethernet.maintain()) {
    case 1:
      // renewed fail
      Serial.println("Error: renewed fail");
      break;

    case 2:
      // renewed success
      Serial.println("Renewed success");
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
      break;

    case 3:
      // rebind fail
      Serial.println("Error: rebind fail");
      break;

    case 4:
      // rebind success
      Serial.println("Rebind success");
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
      break;

    default:
      // nothing happened
      break;
  }
}