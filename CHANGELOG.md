# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - YYYY-MM-DD

### Changed

- cedcwercfe tycrty54 ryc4

### Added

- smthng

### Removed

- clsdjk sdfh sdfjh f

[unreleased]: https://gitlab.com/rakso/avr-http-request-body-thermal-printer/-/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/rakso/avr-http-request-body-thermal-printer/-/tags/v1.0.0

###### (changelog [template](https://wiki.escaperoomsupplier.com/templates/changelog-template#changelog_template_v100) v1.0.0)
